FROM phpdockerio/php74-fpm

ENV DEBIAN_FRONTEND noninteractive

COPY install_npm.sh /install_npm.sh

RUN apt-get update && apt-get -y upgrade \
    && apt-get -y --no-install-recommends install openssh-client git apt-utils mysql-client \
    dialog mc nano telnet php7.4-mysql php7.4-redis php7.4-gd php7.4-bcmath php7.4-bz2 \
    php7.4-geoip php7.4-intl php7.4-mbstring php7.4-tidy php7.4-soap php7.4-imagick php7.4-zip \
    php-pear php7.4-dev libcurl3-openssl-dev php-pear php-dev libzip-dev \
    libfontconfig ttf-mscorefonts-installer iputils-ping nano less build-essential \
    && chmod +x /install_npm.sh && /install_npm.sh \
    && apt-get clean; rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/*

WORKDIR "/var/www/php"
